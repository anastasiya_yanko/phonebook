#import <Foundation/Foundation.h>

@class Contact;

@interface NetworkDataManager : NSObject

+ (instancetype)instance;

- (void)loginAsOwnerWithCompleteBlock:(void (^)(void))aCompleteBlock errorBlock:(void (^)(NSError*))aErrorBlock;
- (BOOL)isLoggedIn;

- (void)loadAllContactsWithSuccessBlock:(void (^)(NSArray* aContacts))aSuccessBlock;
- (void)addContact: (Contact*) contact;
- (void)editContactWithOld:(Contact*)oldContact  updated:(Contact*)newContact;
- (void)deleteContact:(Contact*)contact;

@end
