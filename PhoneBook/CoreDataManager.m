#import "CoreDataManager.h"
#import <CoreData/CoreData.h>
#import "ContactObject+CoreDataProperties.h"
#import "Contact.h"

@interface CoreDataManager()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation CoreDataManager

+ (instancetype)instance
{
    static CoreDataManager *_self = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      _self = [[self alloc] init];
                  });
    return _self;
}

- (NSArray*)loadData
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([ContactObject class])];
    
    NSError *error = nil;
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (!results) {
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        return nil;
    }

    return results;
}

- (NSArray*)loadContactList
{
    NSArray* data = [self loadData];
    
    NSMutableArray* contactsArray = [NSMutableArray array];
    for (ContactObject* object in data)
    {
        Contact* contact = [self parseContactObject:object];
        [contactsArray addObject:contact];
    }
    
    return contactsArray;
}

- (Contact*)parseContactObject:(ContactObject*)aObject
{
    Contact* contact = [[Contact alloc] init];
    
    contact.contactID   = aObject.contactID;
    contact.name        = aObject.name;
    contact.surname     = aObject.surname;
    contact.number      = aObject.number;
    contact.photo       = [UIImage imageWithData:aObject.photo];
    
    return contact;
}

- (void)addContact:(Contact*)aContact
{
    if (aContact.contactID == nil)
    {
        NSLog(@"Error editContact: contactID == nil");
        return;
    }
    
    ContactObject* object = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([ContactObject class])
                                                     inManagedObjectContext:self.managedObjectContext];
    
    object.contactID = aContact.contactID;
    object.name = aContact.name;
    object.surname = aContact.surname;
    object.number = aContact.number;
    object.photo = [aContact imageData];
    
    [self saveContext];
}

- (void)deleteContact:(Contact*)aContact
{
    if (aContact.contactID == nil)
    {
        NSLog(@"Error editContact: contactID == nil");
        return;
    }
    
    NSArray* data = [self loadData];
    for (ContactObject* object in data)
    {
        if ([object.contactID isEqualToString:aContact.contactID])
        {
            [self.managedObjectContext deleteObject:object];
            [self saveContext];
            break;
        }
    }
}

- (void)editContactWithOld:(Contact*)oldContact  updated:(Contact*)newContact
{
    if (oldContact.contactID == nil)
    {
        NSLog(@"Error editContact: contactID == nil");
        return;
    }
    
    NSArray* data = [self loadData];
    for (ContactObject* object in data)
    {
        if ([object.contactID isEqualToString:oldContact.contactID])
        {
            
            if ([oldContact.name isEqualToString:newContact.name] == NO)
                object.name = newContact.name;
            
            if ([oldContact.surname isEqualToString:newContact.surname] == NO)
                object.surname = newContact.surname;
            
            if ([oldContact.number isEqualToString:newContact.number] == NO)
                object.number = newContact.number;
            
            if ([[oldContact imageData] isEqual:[newContact imageData]] == NO)
                object.photo = [newContact imageData];
            
            [self saveContext];
            break;
        }
    }
}

- (void)addContactList:(NSArray*)aList
{
    for (Contact* contact in aList)
    {
        [self addContact:contact];
    }
}

- (void)deleteAllContacts
{
    NSArray* data = [self loadData];
    for (ContactObject* object in data)
    {
        [self.managedObjectContext deleteObject:object];
    }
    
    [self saveContext];
}

#pragma mark - Core Data stack

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
