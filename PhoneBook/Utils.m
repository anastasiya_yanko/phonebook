#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Utils.h"

const NSUInteger kAppID = 39543;
NSString *const kAuthKey        = @"H5x3Rezuy-NxVXL";
NSString *const kAuthSecret     = @"mHZRg5qxD6GPfjP";
NSString *const kAccountKey     = @"qzSSxfCTGLv6SRHsJLUo";

NSString *const kContactClassName        = @"Contact";
NSString *const kContactObjectNameKey    = @"name";
NSString *const kContactObjectSurnameKey = @"surname";
NSString *const kContactObjectNumberKey  = @"number";
NSString *const kContactObjectPhotoKey   = @"photo";

NSString* const kDidLoadContactsNotification = @"kDidLoadContactsNotification";
NSString* const kDidLoadPhotoNotification    = @"kDidLoadPhotoNotification";
NSString* const kNotificationKey_ID    = @"kNotificationKey_ID";
NSString* const kNotificationKey_Data    = @"kNotificationKey_Data";

const CGFloat kMaxImageSide = 400.0f;