#import <Foundation/Foundation.h>

@class Contact;

@interface CoreDataManager : NSObject

+ (instancetype)instance;
- (void)addContact:(Contact*)aContact;
- (void)deleteContact:(Contact*)aContact;
- (void)editContactWithOld:(Contact*)oldContact  updated:(Contact*)newContact;
- (NSArray*)loadContactList;

- (void)addContactList:(NSArray*)aList;
- (void)deleteAllContacts;

@end
