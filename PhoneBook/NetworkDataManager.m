#import "NetworkDataManager.h"
#import <Quickblox/Quickblox.h>
#import <SVProgressHUD.h>
#import "Contact.h"
#import "Utils.h"

@implementation NetworkDataManager

+ (instancetype)instance
{
    static NetworkDataManager *_self = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      _self = [[self alloc] init];
                  });
    return _self;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [QBSettings setApplicationID:kAppID];
        [QBSettings setAuthKey:kAuthKey];
        [QBSettings setAuthSecret:kAuthSecret];
        [QBSettings setAccountKey:kAccountKey];
        
        [QBSettings setLogLevel:QBLogLevelErrors];
    }
    return self;
}

- (void)loginAsOwnerWithCompleteBlock:(void (^)(void))aCompleteBlock errorBlock:(void (^)(NSError*))aErrorBlock;
{
    [QBRequest logInWithUserEmail:@"anastasiya.yanko@gmail.com" password:@"qNhsaGSiCQdsZyWyZM2S" successBlock:^(QBResponse *response, QBUUser *user)
    {
        aCompleteBlock();
    }
    errorBlock:^(QBResponse *response)
    {
        aErrorBlock(response.error.error);
    }];
}

- (BOOL)isLoggedIn
{
    return ([QBSession currentSession].currentUser != nil);
}

- (void)loadAllContactsWithSuccessBlock:(void (^)(NSArray* aContacts))aSuccessBlock
{
    __weak typeof(self) weakSelf = self;
    
    [QBRequest objectsWithClassName:kContactClassName successBlock:^(QBResponse * _Nonnull response, NSArray * _Nullable objects)
    {
        NSMutableArray* resultArray = [NSMutableArray array];
        
        for (QBCOCustomObject* object in objects)
        {
            Contact* contact = [[Contact alloc] init];
            contact.contactID = object.ID;
            contact.name      = [object.fields objectForKey:kContactObjectNameKey];
            contact.surname   = [object.fields objectForKey:kContactObjectSurnameKey];
            contact.number    = [object.fields objectForKey:kContactObjectNumberKey];
            
            [weakSelf downloadImageForContact:contact];
            
            [resultArray addObject:contact];
        }
        
        aSuccessBlock(resultArray);
    } errorBlock:^(QBResponse * _Nonnull response)
    {
        
    }];
}

- (void) addContact: (Contact*) contact
{
    QBCOCustomObject *object = [QBCOCustomObject customObject];
    object.className = kContactClassName;
    
    [object.fields setObject:contact.name       forKey:kContactObjectNameKey];
    [object.fields setObject:contact.surname    forKey:kContactObjectSurnameKey];
    [object.fields setObject:contact.number     forKey:kContactObjectNumberKey];
    
    [QBRequest createObject:object successBlock:^(QBResponse *response, QBCOCustomObject *object)
    {
        contact.contactID = object.ID;
        if (contact.photo != nil)
            [self uploadImageForContact:contact];
        
    } errorBlock:^(QBResponse *response)
    {
        // error handling
        NSLog(@"Response error: %@", [response.error description]);
    }];
}

- (void) editContactWithOld:(Contact*)oldContact updated:(Contact*)newContact
{
    if (oldContact.contactID == nil)
    {
        NSLog(@"Error editContact: contactID == nil");
        return;
    }
    
    QBCOCustomObject *object = [QBCOCustomObject customObject];
    object.className = kContactClassName;
    
    if ([oldContact.name isEqualToString:newContact.name] == NO)
        [object.fields setObject:newContact.name forKey:kContactObjectNameKey];
    
    if ([oldContact.surname isEqualToString:newContact.surname] == NO)
        [object.fields setObject:newContact.surname forKey:kContactObjectSurnameKey];
    
    if ([oldContact.number isEqualToString:newContact.number] == NO)
        [object.fields setObject:newContact.number forKey:kContactObjectNumberKey];
    
    if (newContact.photo != nil)
    {
        if (oldContact.photo != newContact.photo)
        {
            [self uploadImageForContact:newContact];
        }
    }
    else if (oldContact.photo != nil)
    {
        [self deleteImageForContact:oldContact];
    }
    
    object.ID = oldContact.contactID;
    
    [QBRequest updateObject:object successBlock:^(QBResponse *response, QBCOCustomObject *object)
    {
        // object updated
    } errorBlock:^(QBResponse *response)
    {
        // error handling
        NSLog(@"Response error: %@", [response.error description]);
    }];
}

- (void) deleteContact:(Contact*)contact
{
    if (contact.contactID == nil)
    {
        NSLog(@"Error deleteContact: contactID == nil");
        return;
    }
    
    [QBRequest deleteObjectWithID:contact.contactID className:kContactClassName successBlock:^(QBResponse *response)
    {
        // object deleted
        NSLog(@"Contact deleted");
    } errorBlock:^(QBResponse *error)
    {
        // error handling
        NSLog(@"Response error: %@", [error description]);
    }];
}

- (void)deleteImageForContact:(Contact*)aContact
{
    [QBRequest deleteFileFromClassName:kContactClassName objectID:aContact.contactID fileFieldName:kContactObjectPhotoKey successBlock:^(QBResponse *response)
    {
        // file deleted
        NSLog(@"Image deleted");
    } errorBlock:^(QBResponse *error)
    {
        // error handling
        NSLog(@"Response error: %@", [error description]);
    }];
}

- (void)downloadImageForContact:(Contact*)aContact
{
    [QBRequest downloadFileFromClassName:kContactClassName objectID:aContact.contactID fileFieldName:kContactObjectPhotoKey
                            successBlock:^(QBResponse *response, NSData *loadedData)
                            {
                                if (aContact.contactID != nil && loadedData != nil)
                                {
                                    NSLog(@"Image downloaded");
                                    // file downloaded
                                    [[NSNotificationCenter defaultCenter] postNotificationName:kDidLoadPhotoNotification
                                                                                        object:nil
                                                                                      userInfo:@{kNotificationKey_ID  : aContact.contactID,
                                                                                                 kNotificationKey_Data : loadedData}];
                                }
                                
                            }
                             statusBlock:nil
                             errorBlock:^(QBResponse *error)
                            {
                                // error handling
                                NSLog(@"Response error: %@", [error description]);
                            }];
}

- (void)uploadImageForContact:(Contact*)aContact
{
    QBCOFile *file = [QBCOFile file];
    file.name = kContactObjectPhotoKey;
    file.contentType = @"image/png";
    file.data = [aContact imageData];
        
    [QBRequest uploadFile:file className:kContactClassName objectID:aContact.contactID fileFieldName:kContactObjectPhotoKey
                successBlock:^(QBResponse * _Nonnull response, QBCOFileUploadInfo * _Nullable info)
                {
                    NSLog(@"Image uploaded");
                }
                statusBlock:nil
                errorBlock:^(QBResponse * _Nonnull response)
                {
                    NSLog(@"Response error: %@", [response.error description]);
                }];
    
}

@end
