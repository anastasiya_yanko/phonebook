#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Contact : NSObject <NSCoding>


- (BOOL)isEqual:(id)other;
- (NSComparisonResult)compare:(Contact*)otherObject;

@property (nonatomic, copy) NSString* contactID;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* surname;
@property (nonatomic, copy) NSString* number;
@property (nonatomic, copy) UIImage* photo;

- (NSData*)imageData;

@end
