#import "PhoneBook.h"
#import "Contact.h"
#import "NetworkDataManager.h"
#import "Utils.h"
#import "CoreDataManager.h"

@implementation PhoneBook

+ (instancetype)instance
{
    static PhoneBook *_self = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        _self = [[self alloc] init];
    });
    return _self;
}

- (instancetype)init
{
    if (self = [super init])
    {
        _contacts = [NSMutableDictionary dictionary];
        
        [self loadData];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLoadPhoto:) name:kDidLoadPhotoNotification object:nil];
    }

    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) addContact: (Contact*) contact
{
    if ([contact.name length] > 0 || [contact.surname length] > 0)
    {
        NSString* firstLetter = [self getFirstLetter:contact];
        
        if ([self.contacts objectForKey:firstLetter] == nil)
        {
            NSMutableArray* contacts = [NSMutableArray array];
            [self.contacts setObject:contacts forKey:firstLetter];
        }
        
        if ([self.contacts[firstLetter] containsObject:contact] == false)
        {
            [self.contacts[firstLetter] addObject:contact];
            self.contacts[firstLetter] = [[self.contacts[firstLetter] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            
        }
        
        [[NetworkDataManager instance] addContact:contact];
        [[CoreDataManager instance] addContact:contact];
    }
}

- (void) editContactWithOld:(Contact*)oldContact edited:(Contact*)newContact
{
    if (oldContact == newContact || [oldContact isEqual:newContact])
        return;
    
    [[NetworkDataManager instance] editContactWithOld:oldContact updated:newContact];
    [[CoreDataManager instance] editContactWithOld:oldContact updated:newContact];

    if([[self getFirstLetter:oldContact] isEqualToString:[self getFirstLetter:newContact]] == false)
    {
        NSString* key = [self findKeyForContact:oldContact];
        [[self.contacts objectForKey:key] removeObject:oldContact];
        
        NSString* firstLetter = [self getFirstLetter:newContact];
        
        if ([self.contacts objectForKey:firstLetter] == nil)
        {
            NSMutableArray* contacts = [NSMutableArray array];
            [self.contacts setObject:contacts forKey:firstLetter];
        }
        
        if ([self.contacts[firstLetter] containsObject:newContact] == false)
        {
            [self.contacts[firstLetter] addObject:newContact];
            self.contacts[firstLetter] = [[self.contacts[firstLetter] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            
        }
        
    }
    else
    {
        oldContact.name       = newContact.name;
        oldContact.surname    = newContact.surname;
        oldContact.number     = newContact.number;
        oldContact.photo      = newContact.photo;
        
        NSString* key = [self findKeyForContact:oldContact];
        self.contacts[key] = [[self.contacts[key] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
    }
    

}

- (void) deleteContact:(Contact*) contact
{
    NSString* key = [self findKeyForContact:contact];
    [[self.contacts objectForKey:key] removeObject:contact];
    [[NetworkDataManager instance] deleteContact:contact];
    [[CoreDataManager instance] deleteContact:contact];
}

- (void)loadData
{
    __weak typeof(self) weakSelf = self;
    NSArray* contactsArray = [[CoreDataManager instance] loadContactList];
    self.contacts = [[self convertContactsToDictionary:contactsArray] mutableCopy];
    
    [[NetworkDataManager instance] loadAllContactsWithSuccessBlock:^(NSArray *aContacts)
    {
        [[CoreDataManager instance] deleteAllContacts];
        [[CoreDataManager instance] addContactList:aContacts];
        weakSelf.contacts = [[weakSelf convertContactsToDictionary:aContacts] mutableCopy];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidLoadContactsNotification object:nil];
    }];
}

- (NSDictionary*)convertContactsToDictionary:(NSArray*)aContacts
{
    NSMutableDictionary* resultDict = [NSMutableDictionary dictionary];
    
    for (Contact* contact in aContacts)
    {
        NSString* firstLetter = [self getFirstLetter:contact];
        if (firstLetter == nil)
            continue;
        
        if ([resultDict objectForKey:firstLetter] == nil)
        {
            NSMutableArray* contacts = [NSMutableArray array];
            [resultDict setObject:contacts forKey:firstLetter];
        }
        
        if ([resultDict[firstLetter] containsObject:contact] == false)
        {
            [resultDict[firstLetter] addObject:contact];
            resultDict[firstLetter] = [[resultDict[firstLetter] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            
        }
    }
    
    return resultDict;
}

- (NSString*)findKeyForContact: (Contact*) contact
{
    NSString* key;
    
    for (NSArray* array in [self.contacts allValues])
    {
        if ([array containsObject:contact])
        {
            key = [[self.contacts allKeysForObject:array] firstObject];
            break;
        }
    }
    return key;
}

-(NSString*)getFirstLetter: (Contact*) contact
{
    NSString* firstLetter;
    if ([contact.surname length] > 0)
    {
        firstLetter = [contact.surname substringWithRange:NSMakeRange(0, 1)];
    }
    else
    {
        firstLetter = [contact.name substringWithRange:NSMakeRange(0, 1)];
    }
    firstLetter = [firstLetter uppercaseString];
    
    return firstLetter;
}

- (Contact*)findContactByID:(NSString*)aID
{
    for (NSArray* contactsArray in [self.contacts allValues])
    {
        for (Contact* contact in contactsArray)
        {
            if ([contact.contactID isEqualToString:aID])
            {
                return contact;
            }
        }
    }
    return nil;
}

#pragma mark - Notificatins

- (void)didLoadPhoto:(NSNotification* )notification
{
    NSDictionary* userInfo = notification.userInfo;
    
    NSString* contactID = [userInfo objectForKey:kNotificationKey_ID];
    NSData* imageData = [userInfo objectForKey:kNotificationKey_Data];
    
    Contact* contact = [self findContactByID:contactID];
    if (contact != nil)
    {
        UIImage* image = [UIImage imageWithData:imageData];
        contact.photo = image;
    }
    

}

@end

