#import "ContactObject+CoreDataProperties.h"

@implementation ContactObject (CoreDataProperties)

@dynamic name;
@dynamic surname;
@dynamic number;
@dynamic contactID;
@dynamic photo;

@end
