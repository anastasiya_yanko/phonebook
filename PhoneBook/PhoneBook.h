#import <Foundation/Foundation.h>

@class Contact;

@interface PhoneBook : NSObject

+ (instancetype)instance;
- (void) addContact: (Contact*) contact;
- (void) editContactWithOld:(Contact*)oldContact edited:(Contact*)newContact;
- (void) deleteContact:(Contact*) contact;

@property (nonatomic, strong) NSMutableDictionary* contacts;

@end
