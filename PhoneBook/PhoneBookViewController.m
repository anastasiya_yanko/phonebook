#import "PhoneBookViewController.h"
#import "AddContactViewController.h"
#import "PhoneBook.h"
#import "Contact.h"
#import "NetworkDataManager.h"
#import "Utils.h"
#import <SVProgressHUD.h>

@interface PhoneBookViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

static NSString* const kCellID = @"ContactCell";

@implementation PhoneBookViewController

NSMutableArray *sortedContacts;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellID];

    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"+" style:UIBarButtonItemStyleDone target:self action:@selector(switchToAddPage)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    self.searchBar.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLoadContacts:) name:kDidLoadContactsNotification object:nil];
    
    [SVProgressHUD showWithStatus:@"Ligging in..."];
    [[NetworkDataManager instance] loginAsOwnerWithCompleteBlock:^
     {
         [SVProgressHUD dismiss];
         NSLog(@"logInWithUserEmail");
         
     }
     errorBlock:^(NSError *aError)
     {
         [SVProgressHUD showErrorWithStatus:@"Failed to login!"];
         NSLog(@"Response error %@:", aError);
     }];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateContactsList];
}

- (void)updateContactsList
{
    NSArray *sortedKeys = [[[PhoneBook instance].contacts allKeys] sortedArrayUsingSelector: @selector(localizedCaseInsensitiveCompare:)];
    sortedContacts = [NSMutableArray array];
    for (NSString *key in sortedKeys)
    {
        if([[[PhoneBook instance].contacts objectForKey:key] count] > 0)
        {
            [sortedContacts addObject:[[PhoneBook instance].contacts objectForKey:key]];
        }
    }
    
    self.searchBar.text = @"";
    self.isFiltered = false;
    [self.tableView reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)hideKeyboard:(UIButton *)sender
{
    [self.searchBar resignFirstResponder];
    [sender removeFromSuperview];
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray* keys = [[NSMutableArray	alloc] init];
    
    if(self.isFiltered == false)
    {
        NSArray* contacts = [[PhoneBook instance].contacts allValues];
        
        for (int i = 0; i < [contacts count]; ++i)
        {
            if ([contacts[i] count] > 0)
            {
                NSString *key = [[[PhoneBook instance].contacts allKeysForObject:contacts[i]] firstObject];
                [keys addObject:key];
            }
        }
        
        [keys sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    
    return keys;
}

- (void)switchToAddPage
{
    AddContactViewController *viewController = [[AddContactViewController alloc] initWithNibName:@"AddContactViewController" bundle:nil];
    viewController.title = @"Add Contact";
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)switchToEditPage:(Contact*)contact
{
    AddContactViewController *viewController = [[AddContactViewController alloc] initWithContact:contact];
    viewController.title = @"Edit Contact";
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.isFiltered)
    {
        return 1;
    }
    else
    {
        return sortedContacts.count;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger rowCount = 0;
    if(self.isFiltered)
        rowCount = self.filteredContactList.count;
    else
    {
        NSArray* contacts = [sortedContacts objectAtIndex:section];
        rowCount = contacts.count;
    }
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    
    NSArray* contacts = [sortedContacts objectAtIndex:indexPath.section];
    
    Contact* contact;
    if(self.isFiltered)
        contact = [self.filteredContactList objectAtIndex:indexPath.row];
    else
        contact = contacts[indexPath.row];
    
    NSString* string;
    if([contact.surname length] == 0)
        string = contact.name;
    else
        string = [NSString stringWithFormat:@"%@ %@", contact.surname, contact.name];
    
    cell.textLabel.text = string;
    
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* header;
    
    if (self.isFiltered == false)
    {
        NSMutableArray* object = [sortedContacts objectAtIndex:section];
        header = [[[PhoneBook instance].contacts allKeysForObject:object] firstObject];
    }
    
    return header;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Contact* contact;
    if (self.isFiltered)
    {
        contact = self.filteredContactList[indexPath.row];
    }
    else
    {
        NSArray* contacts = [sortedContacts objectAtIndex:indexPath.section];
        contact = contacts[indexPath.row];
    }
    
    [self switchToEditPage:contact];
}

#pragma mark - UISearchBarDelegate

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        self.isFiltered = false;
    }
    else
    {
        self.isFiltered = true;
        self.filteredContactList = [[NSMutableArray alloc] init];
        
        for (NSArray* contacts in [[PhoneBook instance].contacts allValues])
        {
            for (Contact* contact in contacts)
            {
                NSRange nameRange = [contact.name rangeOfString:text options:NSCaseInsensitiveSearch];
                NSRange surnameRange = [contact.surname rangeOfString:text options:NSCaseInsensitiveSearch];
                
                if(nameRange.location != NSNotFound || surnameRange.location != NSNotFound)
                    [self.filteredContactList addObject:contact];
            }
            
        }
    }
    
    [self.tableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    UIButton *overlayButton = [[UIButton alloc] initWithFrame:self.view.frame];
    overlayButton.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3f];
    [overlayButton addTarget:self action:@selector(hideKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:overlayButton];
}

#pragma mark - Notificatins

- (void)didLoadContacts:(NSNotification* )notification
{
    [self updateContactsList];
}

@end
