#import "Contact.h"

@implementation Contact

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.surname forKey:@"surname"];
    [encoder encodeObject:self.number forKey:@"number"];
    NSData* photoData = UIImagePNGRepresentation(self.photo);
    [encoder encodeObject:photoData forKey:@"photo"];
}

- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self)
    {
        self.name = [decoder decodeObjectForKey:@"name"];
        self.surname = [decoder decodeObjectForKey:@"surname"];
        self.number = [decoder decodeObjectForKey:@"number"];
        self.photo = [UIImage imageWithData:[decoder decodeObjectForKey:@"photo"]];
        self.contactID = nil;
    }
    return self;
}

- (BOOL)isEqual:(id)other {
    if (other == self)
        return YES;
    
    if (!other || ![other isKindOfClass:[self class]])
        return NO;
    
    return [self isEqualToContact:other];
}

- (BOOL)isEqualToContact:(Contact*)contact
{
    if (self == contact)
        return YES;
    
    if (![self.name isEqualToString:contact.name] ||
        ![self.surname isEqualToString:contact.surname] ||
        ![self.number isEqualToString:contact.number] ||
        ![self.contactID isEqualToString:contact.contactID] ||
        ![UIImagePNGRepresentation(self.photo) isEqual:UIImagePNGRepresentation(contact.photo)])
        return NO;

    return YES;
}

- (NSUInteger)hash
{
    NSUInteger hash = 0;
    hash += [[self name] hash];
    hash += [[self surname] hash];
    hash += [[self number] hash];
    hash += [[self contactID] hash];
    return hash;
}

- (NSComparisonResult)compare:(Contact*)otherObject
{
    NSComparisonResult comparison;
    
    if([self.surname length] == 0 || [otherObject.surname length] == 0)
    {
        if([self.surname length] == 0)
            comparison = [self.name caseInsensitiveCompare:otherObject.surname];
        else
            comparison = [self.surname caseInsensitiveCompare:otherObject.name];
    }
    else
    {
        comparison = [self.surname caseInsensitiveCompare:otherObject.surname];
        
        if (comparison == NSOrderedSame)
        {
            comparison = [self.name caseInsensitiveCompare:otherObject.name];
        }
        
    }
    
    
    return comparison;
}

- (NSData*)imageData
{
    return self.photo != nil ? UIImagePNGRepresentation(self.photo) : nil;
}

@end
