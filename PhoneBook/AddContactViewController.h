#import <UIKit/UIKit.h>

@class Contact;

@interface AddContactViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>

- (id) initWithContact:(Contact*)contact;
- (IBAction)addPhoto:(id)sender;

@property (weak, nonatomic) Contact* contact;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *surname;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
