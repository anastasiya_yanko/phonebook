#ifndef Utils_h
#define Utils_h

extern const NSUInteger kAppID;
extern NSString *const kAuthKey;
extern NSString *const kAuthSecret;
extern NSString *const kAccountKey;

extern NSString *const kContactClassName;
extern NSString *const kContactObjectNameKey;
extern NSString *const kContactObjectSurnameKey;
extern NSString *const kContactObjectNumberKey;
extern NSString *const kContactObjectPhotoKey;

extern NSString* const kDidLoadContactsNotification;
extern NSString* const kDidLoadPhotoNotification;
extern NSString* const kNotificationKey_ID;
extern NSString* const kNotificationKey_Data;

extern const CGFloat kMaxImageSide;

#endif /* Utils_h */
