#import "AddContactViewController.h"
#import "PhoneBook.h"
#import "Contact.h"
#import "Utils.h"

@interface AddContactViewController ()

@end

@implementation AddContactViewController

- (id) initWithContact:(Contact*)contact
{
    if (self = [super initWithNibName:nil bundle: nil])
    {
        self.contact = contact;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style: UIBarButtonItemStyleDone target:self action:@selector(switchPage)];
    self.navigationItem.leftBarButtonItem = backButton;
    UIBarButtonItem *editButton;
    
    if (self.contact != nil)
    {
        editButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(editContact)];
        self.deleteButton.hidden = NO;
        
        self.name.text = self.contact.name;
        self.surname.text = self.contact.surname;
        self.phoneNumber.text = self.contact.number;
        self.imageView.image = self.contact.photo;
    }
    else
    {
        editButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleDone target:self action:@selector(addContact)];
    }

    self.navigationItem.rightBarButtonItem = editButton;
    
    self.name.delegate = self;
    self.surname.delegate = self;
    self.phoneNumber.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)addContact
{
    
    if([self.name.text isEqualToString:@""] && [self.surname.text isEqualToString:@""])
    {
        // There's no text in the box.
        [self.name setBackgroundColor:[UIColor redColor]];
        [self.surname setBackgroundColor:[UIColor redColor]];
        return;
    }
    if ([self.phoneNumber.text length] > 0 && [self validatePhoneNumber:self.phoneNumber.text] == false)
    {
        [self.phoneNumber setBackgroundColor:[UIColor redColor]];
        return;
    }
    
    Contact* contact = [[Contact alloc] init];
    
    contact.contactID   = self.contact.contactID;
    contact.name        =  [self trimString:self.name.text];
    contact.surname     = [self trimString:self.surname.text];
    contact.number      = [self trimString:self.phoneNumber.text];
    contact.photo       = self.imageView.image;
    
    [[PhoneBook instance] addContact:contact];
    
    [self switchPage];
}

- (void)editContact
{
    if ([self.phoneNumber.text length] > 0 && [self validatePhoneNumber:self.phoneNumber.text] == false)
    {
        [self.phoneNumber setBackgroundColor:[UIColor redColor]];
        return;
    }
    
    if([self.name.text isEqualToString:@""] && [self.surname.text isEqualToString:@""])
    {
        // There's no text in the box.
        [self.name setBackgroundColor:[UIColor redColor]];
        [self.surname setBackgroundColor:[UIColor redColor]];
        return;
    }
    
    Contact* newContact = [[Contact alloc] init];
    
    newContact.contactID = self.contact.contactID;
    newContact.name      = [self trimString:self.name.text];
    newContact.surname   = [self trimString:self.surname.text];
    newContact.number    = [self trimString:self.phoneNumber.text];
    newContact.photo     = self.imageView.image;
    
    [[PhoneBook instance] editContactWithOld:self.contact edited:newContact];
    
    [self switchPage];
}

- (IBAction)deleteContact:(id)sender
{
    [[PhoneBook instance] deleteContact:self.contact];
    [self switchPage];
}

- (void)switchPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addPhoto:(id)sender
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImage* newImage = [self compressImage:image];
    self.imageView.image = newImage;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSString*) trimString: (NSString*)string
{
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return trimmedString;
}

-(BOOL) validatePhoneNumber:(NSString*)phoneNumber
{
    //NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSString *phoneRegex = @"[0-9]{6}([0-9]{4})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}

- (UIImage*)compressImage:(UIImage*)aImage
{
    CGSize imageSize = aImage.size;
    
    CGFloat largeImageSide = MAX(imageSize.width, imageSize.height);
    
    if (largeImageSide <= kMaxImageSide)
        return aImage;
    
    CGFloat scale = kMaxImageSide / largeImageSide;
    
    CGSize newSize = CGSizeMake(imageSize.width * scale, imageSize.height * scale);
    
    UIGraphicsBeginImageContext(newSize);
    [aImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
