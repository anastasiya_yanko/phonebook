#import <UIKit/UIKit.h>

@interface PhoneBookViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, assign) BOOL isFiltered;

@property (strong, nonatomic) NSMutableArray *filteredContactList;

@end
