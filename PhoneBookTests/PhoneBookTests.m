#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "CoreDataManager.h"
#import "PhoneBook.h"
#import "Contact.h"

@interface PhoneBookTests : XCTestCase

@end

@implementation PhoneBookTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    sleep(5);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (Contact*)randomContact
{
    Contact* contact = [[Contact alloc] init];
    contact.contactID = [NSString stringWithFormat:@"%d", (arc4random() % 100000)];
    contact.name = @[@"User", @"Alexa", @"John", @"Tim"][arc4random() % 4];
    contact.surname = @[@"Obama", @"Gates", @"Clinton", @"Lennon"][arc4random() % 4];
    
    return contact;
}

#pragma mark - Core Data tests

- (void)testAddObjectToCoreData
{

    NSArray* testArray = [[CoreDataManager instance] loadContactList];
    Contact* contact = [self randomContact];
    [[CoreDataManager instance] addContact:contact];
    
    NSArray* newTestArray = [[CoreDataManager instance] loadContactList];
    
    XCTAssertTrue(([testArray count] + 1) == [newTestArray count], @"Object hasn't added to Core Data");
    
}

- (void)testDeleteObjectFromCoreData
{
    
    NSArray* testArray = [[CoreDataManager instance] loadContactList];
    Contact* contact1 = [self randomContact];
    Contact* contact2 = [self randomContact];
    Contact* contact3 = [self randomContact];
    [[CoreDataManager instance] addContact:contact1];
    [[CoreDataManager instance] addContact:contact2];
    [[CoreDataManager instance] addContact:contact3];
    
    [[CoreDataManager instance] deleteContact:contact2];

    NSArray* newTestArray = [[CoreDataManager instance] loadContactList];
    
    XCTAssertTrue(([testArray count] + 2) == [newTestArray count], @"Object hasn't removed from Core Data");
    
}

- (void)testEditObjectFromCoreData
{    
    Contact* contact1 = [self randomContact];
    [[CoreDataManager instance] deleteAllContacts];
    [[CoreDataManager instance] addContact:contact1];

    Contact* newContact = [[[CoreDataManager instance] loadContactList] lastObject];
    NSString* newName = @"Edit Name";
    newContact.name = newName;
    
    [[CoreDataManager instance] editContactWithOld:contact1 updated:newContact];
    
    Contact* newContact2 = [[[CoreDataManager instance] loadContactList] lastObject];
    
    XCTAssertTrue([newContact2.name isEqualToString:newName], @"Object hasn't removed from Core Data");
    
}

@end
